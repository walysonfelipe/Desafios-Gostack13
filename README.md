<img src="https://camo.githubusercontent.com/d25397e9df01fe7882dcc1cbc96bdf052ffd7d0c/68747470733a2f2f73746f726167652e676f6f676c65617069732e636f6d2f676f6c64656e2d77696e642f626f6f7463616d702d676f737461636b2f6865616465722d6465736166696f732e706e67">

<center>
 <h1>Desafios do Bootcamp GoStack 13 - RocketSeat</h1>
</center>

<h4> 📌 Lista de Desafios Concluidos: </h4>


| Índice | README |
| ------ | ------ |
| 00 | [desafio-conceitos-node](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafio-conceitos-node) |
| 01 | [desafio-conceitos-react-native](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafio-conceitos-react-native) |
| 02 | [desafio-conceitos-reactjs](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafio-conceitos-reactjs) |
| 03 | [desafio-database-relations](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafio-database-relations) |
| 04 | [desafio-database-upload](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafio-database-upload) |
| 05 | [desafio-fundamentos-react](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafio-fundamentos-reactjs) |
| 06 | [desafio-gorestaurant-mobile](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafio-gorestaurant-mobile) |
| 07 | [desafio-gorestaurant-web](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafio-gorestaurant-web) |
| 08 | [desafio-primeiro-projeto-node-js](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafio-primeiro-projeto-node-js) |
| 09 | [desafios-fundamentos-react-native](https://github.com/walysonfelipe/Desafios-Gostack13/tree/main/desafios-fundamentos-react-native) |

## 🌈 Criado por:<br>
<table>
  <tr>
   <td align="center">
      <a href="https://github.com/walysonfelipe">
        <img src="https://avatars1.githubusercontent.com/u/35854466" width="100px;" alt="Foto do <Waly> no GitHub"/><br>
        <sub>
          <b><<!---->Walyson></b>
        </sub>
      </a><br>
   </td>
  </tr>
</table>

## ⚖ Licença
Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE.md) para mais detalhes.

 
